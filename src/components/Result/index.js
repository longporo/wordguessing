// update by Yi Song 2021/12/3
// add material UI
// add map with UI changing for list result data

// update by Yi Song 2021/12/5
// modify buttion onclick back to root

import * as React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

class ResultPage extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        // the items passed from gaming page
        const items = this.props.location.items || [];
        return (
            <div>
            <Box m="auto" bgcolor="lightgreen" alignItems="center" justifyContent="center">
                <Typography align="center" variant="h3" gutterBottom component="div">
                     SCORE Result
                </Typography>
            </Box>  
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 0 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center"><b><font color="blue">Chinese Word</font></b></TableCell>
                  <TableCell align="center"><b><font color="blue">English Word</font></b></TableCell>
                  <TableCell align="center"><b><font color="blue">Total number of wrong answers</font></b></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {items.sort((a, b) => a.wrongCount < b.wrongCount ? 1 : -1).map((row) => (
                  <TableRow
                    key={row.DC_ENG}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align="center" component="th" scope="row">
                      {row.DC_ZH}
                    </TableCell>
                    <TableCell align="center">{row.DC_ENG}</TableCell>
                    <TableCell align="center">
                      {row.wrongCount>0 ? <font color="red">{row.wrongCount}</font>
                      : <font color="blue">{row.wrongCount}</font>}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <br />
          <br />
          <Button variant="contained" onClick={() => { window.location.href = '/';}} disableElevation>
             Back - Enjoy the Game - Try again !
          </Button>
          <br />
          <br />
          <br />
          <br />
          </div>
        );
    }
}

export default (ResultPage);