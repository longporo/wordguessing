// update by Yi Song 2021/12/4
// Fetch Firebase Data
// add map with UI changing
import * as React from 'react';
import {withFirebase} from '../Firebase';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import * as ROUTES from '../../constants/routes';
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import styled from "@material-ui/core/styles/styled";
import Grid from "@material-ui/core/Grid";

const Block = styled(Grid)(({theme}) => ({
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center'
}));

const Root = styled('div')(({theme}) => ({
    width: '100%',
    margin: 30,
    ...theme.typography.body2,
    '& > :not(style) + :not(style)': {
        marginTop: theme.spacing(2),
    },
}));

class UnitsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
        };
    }

    componentDidMount() {
        const {firebase} = this.props;
        let bind = this.props.location.bind - 1;
        let eind = this.props.location.eind - 1;
        // get units
        firebase.getIndexByRange(bind.toString(), eind.toString()).then((value) => {
            value.map((item, index) => {
                item.index = index;
            });
            this.setState({
                items: value,
            });
        });
    }

    componentWillUnmount() {
    }

    render() {
        const items = this.state.items || [];
        return (
            <div align="center">
                <Block container>
                    <Root>
                        <div style={{width: "60%", marginLeft: "20%", textAlign: "center"}}>
                            <Divider>
                                <Typography variant="h5" component="h5">
                                    Choose your Unit!
                                </Typography>
                            </Divider>
                        </div>
                    </Root>
                </Block>
                <Box sx={{'& button': {m: 2}}}>
                    {items.map((row) => (
                        <Button variant="contained" sx={{mr: 3}} component={Link}
                                to={
                                    {
                                        pathname: ROUTES.GAMING,
                                        brid: row.DC_BRID - 1,
                                        erid: row.DC_ERID - 1,
                                    }
                                }>
                            <Typography gutterBottom variant="h6" component="div" sx={{mb: 0}}>
                                UNIT {row.INDEX} {row.ID}
                            </Typography>
                        </Button>
                    ))}
                </Box>
            </div>
        );
    }
}

export default (withFirebase)(UnitsPage);