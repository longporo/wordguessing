// update by Yi Song 2021/12/4
// Add getNavByRange and getIndexByRange for Level and Unit Page Fetch Firebase Data
import {initializeApp} from "firebase/app";
import {getAuth} from 'firebase/auth';
import {getDatabase, ref, onValue, query, orderByKey, startAt, endAt} from 'firebase/database';
import {getStorage} from "firebase/storage";
import * as MyUtils from "../../common/myUtils";
const firebaseConfig = {
    apiKey: "AIzaSyB3x5Mc2PS9ESwGthS6bMaibEwUhuwD0NI",
    authDomain: "word-game-8df39.firebaseapp.com",
    projectId: "word-game-8df39",
    storageBucket: "word-game-8df39.appspot.com",
    messagingSenderId: "890479608531",
    appId: "1:890479608531:web:1f92be8d9bf7ac5fc4ca26",
    measurementId: "G-ZBW5XKX19N"
};

// Initialize Firebase
class Firebase {
    constructor() {
        const app = initializeApp(firebaseConfig);
        this.auth = getAuth();
        this.db = getDatabase(app);
        this.storage = getStorage(app);
    }

    /**
     * Get items by range
     * @param start
     * @param end
     * @returns {Promise<any>}
     */
    getItemsByRange = (start, end) => {
        return new Promise((resolve, reject) => {
            onValue(query(ref(this.db, "DC_CONTENT/"), orderByKey(), startAt(start), endAt(end)), (snapshot) => {
                const result = snapshot.val();
                let dataList = MyUtils.keyObjToArray(result);
                resolve(dataList);
            },{
                onlyOnce: true
            });
        });
    };

    /**
     * Get levels by range
     * @param start
     * @param end
     * @returns {Promise<any>}
     */
    getNavByRange = (start, end) => {
        return new Promise((resolve, reject) => {
            onValue(query(ref(this.db, "DC_NAV/"), orderByKey(), startAt(start), endAt(end)), (snapshot) => {
                const result = snapshot.val();
                let dataList = MyUtils.keyObjToArray(result);
                resolve(dataList);
            },{
                onlyOnce: true
            });
        });
    };

    /**
     * Get units by range
     * @param start
     * @param end
     * @returns {Promise<any>}
     */
    getIndexByRange = (start, end) => {
        return new Promise((resolve, reject) => {
            onValue(query(ref(this.db, "DC_INDEX/"), orderByKey(), startAt(start), endAt(end)), (snapshot) => {
                const result = snapshot.val();
                let dataList = MyUtils.keyObjToArray(result);
                resolve(dataList);
            },{
                onlyOnce: true
            });
        });
    };
}

export default Firebase;