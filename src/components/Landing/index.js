// update by Yi Song 2021/12/4
// Fetch Firebase Data
// add map with UI changing

import * as React from 'react';
import {withFirebase} from '../Firebase';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import * as ROUTES from '../../constants/routes';
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import styled from "@material-ui/core/styles/styled";
import Grid from "@material-ui/core/Grid";

const Block = styled(Grid)(({theme}) => ({
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center'
}));

const Root = styled('div')(({theme}) => ({
    width: '100%',
    margin: 30,
    ...theme.typography.body2,
    '& > :not(style) + :not(style)': {
        marginTop: theme.spacing(2),
    },
}));

class LandingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            loading: false,
        };
    }

    componentDidMount() {
        const {firebase} = this.props;
        // get levels
        firebase.getNavByRange("0", "2").then((value) => {
            value.map((item, index) => {
                item.index = index;
            });
            this.setState({
                items: value,
            });
        });
    }

    componentWillUnmount() {
    }

    render() {
        const items = this.state.items;
        return (
            <div className="App">
                <div align="center">
                    <Block container>
                        <Root>
                            <div style={{width: "60%", marginLeft: "20%", textAlign: "center"}}>
                                <Divider>
                                    <Typography variant="h5" component="h5">
                                        Choose your level!
                                    </Typography>
                                </Divider>
                            </div>
                        </Root>
                    </Block>
                    <Box sx={{'& button': {m: 2}}}>
                        {items.map((row) => (
                            <div>
                                <Button variant="contained" sx={{mb: 3}} component={Link}
                                        to={
                                            {
                                                pathname: ROUTES.UNITS,
                                                bind: row.DC_BIND,
                                                eind: row.DC_EIND,
                                            }
                                        }>
                                    <h2>LEVEL {row.index + 1}</h2>
                                </Button>
                            </div>
                        ))}
                    </Box>
                    <br/>
                </div>
            </div>
        );
    }
}

export default (withFirebase)(LandingPage);
