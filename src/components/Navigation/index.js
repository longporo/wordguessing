import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import React from "react";
import {Link} from "react-router-dom";

import * as ROUTES from "../../constants/routes";

export default function NavBar() {
    return (
        <Box sx={{flexGrow: 1}}>
            <AppBar position="static">
                <Toolbar variant="dense" sx={{display: 'flex', justifyContent: 'center',}}>
                    <Typography variant="h5" color="inherit" sx={{textDecoration: "none"}} component={Link} to={ROUTES.LANDING}>
                        Word Guessing Game
                    </Typography>
                </Toolbar>
            </AppBar>
        </Box>
    );
}