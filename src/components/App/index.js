import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';

import  Navigation from '../Navigation';
import LandingPage from '../Landing';
import UnitsPage from '../Units';
import GamingPage from '../Gaming';
import ResultPage from '../Result';

import * as ROUTES from '../../constants/routes';
import { createTheme, ThemeProvider } from "@material-ui/core";

const theme = createTheme({
    overrides: {
        MuiButton: {
            root: {
                margin: "10px",
                padding: "10px"
            }
        }
    }
});
class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <ThemeProvider theme={theme}>
                    <Navigation/>
                    <Route exact path={ROUTES.LANDING} component={LandingPage}/>
                    <Route path={ROUTES.UNITS} component={UnitsPage}/>
                    <Route path={ROUTES.GAMING} component={GamingPage}/>
                    <Route path={ROUTES.RESULT} component={ResultPage}/>
                </ThemeProvider>
            </Router>
        );
    }
}

export default App;