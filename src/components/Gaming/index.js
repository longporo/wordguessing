// update by Yi SOng 2021/12/5
// add pass parameters from Units Page
import * as React from 'react';
import {withFirebase} from '../Firebase';
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import Divider from "@material-ui/core/Divider";
import styled from "@material-ui/core/styles/styled";
import Typography from "@material-ui/core/Typography";
import grey from "@material-ui/core/colors/grey";
import CardActions from "@material-ui/core/CardActions";
import Card from "@material-ui/core/Card";
import red from "@material-ui/core/colors/red";
import green from "@material-ui/core/colors/green";
import blue from "@material-ui/core/colors/blue";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import DialogContent from "@material-ui/core/DialogContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardHeader from "@material-ui/core/CardHeader";

import * as ROUTES from '../../constants/routes';

const Block = styled(Grid)(({theme}) => ({
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center'
}));

const Root = styled('div')(({theme}) => ({
    width: '100%',
    margin: 30,
    ...theme.typography.body2,
    '& > :not(style) + :not(style)': {
        marginTop: theme.spacing(2),
    },
}));

const RootItemStyled = styled(Typography)(({theme}) => ({
    paddingTop: "10px",
    paddingBottom: "10px",
    color: grey["600"],
}));

const RootItem = (props) => {
    return (
        <RootItemStyled variant="h5" component="h5">
            {props.value}
        </RootItemStyled>
    )
};

const SubItem = styled(Card)(({theme}) => ({
    ...theme.typography.body2,
    margin: 30,
    display: "inline-block",
    minWidth: 220,
    color: theme.palette.text.secondary,
}));

const CardItemStyled = styled(Card)(({theme}) => ({
    ...theme.typography.body2,
    color: theme.palette.text.secondary,
}));

class GamePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showTips: true,
            loading: true,
            items: [],
            score: 0,
            wrongCount: 0,
            correctCount: 0,
            sx0: {},
            sx1: {},
            sx2: {},
            sx3: {},
            timer: 60,
            dialogOpen: false,
        };
    }

    componentDidMount() {
        const {firebase} = this.props;
        // the range passed from units page
        let brid = this.props.location.brid - 1;
        let erid = this.props.location.erid - 1;
        // get items
        firebase.getItemsByRange(brid.toString(), erid.toString()).then((value) => {
            value.map((item, index) => {
                item.index = index;
                item.wrongCount = 0;
            });
            this.setState({
                loading: false,
                items: value,
            }, () => {
                this.setState({currItem: this.genRandomItem()})
            });
        });
    }

    /**
     * get an item from itemList randomly
     * @param filterIndex: the item excluded
     * @returns {*}
     */
    genRandomItem(filterIndex) {
        let itemList = this.state.items;
        let max = itemList.length - 1;
        let randomIndex = this.getRandomArr(max, filterIndex, 1);
        let item = itemList[randomIndex];
        item.answers = [];
        let randomArr = this.getRandomArr(max, randomIndex, 3);
        randomArr.push(randomIndex);
        let sequenceArr = this.getRandomArr(3, null, 4);
        sequenceArr.map((sequence) => {
            let randomItem = itemList[randomArr[sequence]];
            item.answers.push(randomItem.DC_ENG);
        });
        return item;
    }

    /**
     * Generate a number array which elements are unique and not greater than the max
     * @param max
     * @param filterNum
     * @param length
     * @returns {Array}
     */
    getRandomArr(max, filterNum, length) {
        let randomArr = [];
        while (randomArr.length < length) {
            let num = Math.floor(Math.random() * (max + 1));
            if (filterNum && num == filterNum) {
                continue;
            }
            if (randomArr.indexOf(num) !== -1) {
                continue;
            }
            randomArr.push(num);
        }
        return randomArr;
    }

    componentWillUnmount() {
    }

    /**
     * The click event of start button, start the timer for one minutes
     */
    handleStartClick() {
        this.setState({showTips: false}, () => {
            let countDown = setInterval(() => {
                this.setState({timer: this.state.timer - 1}, () => {
                    if (this.state.timer === 0) {
                        clearInterval(countDown);
                        this.setState({dialogOpen: true});
                    }
                })
            }, 1000);
        });
    }

    /**
     * The click event of answers
     * @param item
     * @param positionIndex
     */
    handleAnswerClick(item, positionIndex) {
        const{score, correctCount, wrongCount} = this.state;
        let scoreTmp = score;
        let update = {};
        if (item.DC_ENG === item.answers[positionIndex]) {
            // correct answer
            scoreTmp = scoreTmp + 5;
            let count = correctCount + 1;
            update.correctCount = count;
            // add green background to the click answer
            update["sx" + positionIndex] = {backgroundColor: green["400"]};
        } else {
            // incorrect answer
            scoreTmp = scoreTmp - 5;
            item.wrongCount = item.wrongCount + 1;
            let count = wrongCount + 1;
            update.wrongCount = count;
            // add red background to the click answer
            update["sx" + positionIndex] = {backgroundColor: red["400"]};
        }
        if (scoreTmp < 0) {
            scoreTmp = 0;
        }
        update.score = scoreTmp;
        // reset the background of the click answer
        this.setState(update, () => {
            setTimeout(() => {
                this.setState({currItem: this.genRandomItem(item.index)}, () => {
                    let update = {};
                    update["sx" + positionIndex] = {};
                    this.setState(update);
                });
            }, 100);
        });
    }

    /**
     * The close event of the time up dialog
     */
    handleDialogClose() {
        this.setState({dialogOpen: false});
        // jump to result page
        let result = {
            pathname: ROUTES.RESULT,
            items: this.state.items
        };
        this.props.history.push(result);
    };

    render() {
        const CardItem = (props) => {
            const {currItem, index, sx} = props;
            return (
                <CardItemStyled sx={sx} variant="outlined">
                    <CardActionArea component={Button} onClick={() => {this.handleAnswerClick(currItem, index)}}>
                        <CardHeader
                            sx={{padding: '10px'}}
                            title={currItem.answers[index]}
                        />
                    </CardActionArea>
                </CardItemStyled>
            )
        };
        const {showTips, loading, items, currItem, score, wrongCount, correctCount, timer, dialogOpen, sx0, sx1, sx2, sx3} = this.state;
        return (
            <Box m={4} mt={2}>
                {!showTips && (
                    <Block container>
                        <Block container>
                            <SubItem>
                                <Typography gutterBottom variant="h4" component="h4">
                                    SCORE
                                </Typography>
                                <Divider light/>
                                <CardActions sx={{paddingTop: "0px"}}>
                                    <Typography gutterBottom variant="h6" component="h6"
                                                sx={{width: "100%"}}>
                                        {score}
                                    </Typography>
                                </CardActions>
                            </SubItem>
                            <SubItem>
                                <Typography gutterBottom variant="h4" component="h4" sx={{color: red["300"]}}>
                                    WRONG
                                </Typography>
                                <Divider light/>
                                <CardActions sx={{paddingTop: "0px"}}>
                                    <Typography gutterBottom variant="h6" component="h6"
                                                sx={{width: "100%", color: red["300"]}}>
                                        {wrongCount}
                                    </Typography>
                                </CardActions>
                            </SubItem>
                            <SubItem>
                                <Typography gutterBottom variant="h4" component="h4" sx={{color: green["300"]}}>
                                    CORRECT
                                </Typography>
                                <Divider light/>
                                <CardActions sx={{paddingTop: "0px"}}>
                                    <Typography gutterBottom variant="h6" component="h6"
                                                sx={{width: "100%", color: green["300"]}}>
                                        {correctCount}
                                    </Typography>
                                </CardActions>
                            </SubItem>
                            <SubItem>
                                <Typography gutterBottom variant="h4" component="h4" sx={{color: blue["300"]}}>
                                    LEFT TIME
                                </Typography>
                                <Divider light/>
                                <CardActions sx={{paddingTop: "0px"}}>
                                    <Typography gutterBottom variant="h6" component="h6"
                                                sx={{width: "100%", color: blue["300"]}}>
                                        {timer} S
                                    </Typography>
                                </CardActions>
                            </SubItem>
                        </Block>
                        <Block container>
                            <SubItem>
                                <Typography gutterBottom variant="h1" component="h1" sx={{marginBottom: 0}}>
                                    {currItem.DC_ZH}
                                </Typography>
                            </SubItem>
                        </Block>
                        <Block container>
                            <SubItem>
                                <CardItem index={"0"} sx={sx0} currItem={currItem}/>
                            </SubItem>
                            <SubItem>
                                <CardItem index={"1"} sx={sx1} currItem={currItem}/>
                            </SubItem>
                        </Block>
                        <Block container>
                            <SubItem>
                                <CardItem index={"2"} sx={sx2} currItem={currItem}/>
                            </SubItem>
                            <SubItem>
                                <CardItem index={"3"} sx={sx3} currItem={currItem}/>
                            </SubItem>
                        </Block>
                    </Block>
                )}
                {showTips && (
                    <Block container>
                        <Root>
                            <div style={{width: "60%", marginLeft: "20%", textAlign: "center"}}>
                                <Divider>
                                    <Typography variant="h5" component="h5">
                                        TIPS
                                    </Typography>
                                </Divider>
                                <div>
                                    <RootItem value={"There will be " + items.length + " items in this unit."}/>
                                    <RootItem value={"You have 1 minute to finish them."}/>
                                    <RootItem value={"Click 'START' and enjoy gaming!"}/>
                                </div>
                            </div>
                        </Root>
                        <Fab color="secondary" disabled={loading} variant="extended" onClick={() => {
                            this.handleStartClick()
                        }}>
                            <Typography variant="h6" component="h6">
                                START
                            </Typography>
                        </Fab>
                    </Block>
                )}
                <Dialog
                    open={dialogOpen}
                    onClose={(event, reason) => {
                        if (reason !== 'backdropClick') {
                            this.handleDialogClose();
                        }
                    }}
                    maxWidth={"xs"}
                    fullWidth={true}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Tips"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {"Time's up!!!"}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => {
                            this.handleDialogClose()
                        }}>Okay</Button>
                    </DialogActions>
                </Dialog>
            </Box>
        );
    }
}

export default (withFirebase)(GamePage);
